﻿namespace PCinfo
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label_ip = new System.Windows.Forms.Label();
            this.textBox_ip = new System.Windows.Forms.TextBox();
            this.label_hostname = new System.Windows.Forms.Label();
            this.textBox_hostname = new System.Windows.Forms.TextBox();
            this.textBox_loginuser = new System.Windows.Forms.TextBox();
            this.label_loginuser = new System.Windows.Forms.Label();
            this.textBox_domainname = new System.Windows.Forms.TextBox();
            this.label_domainname = new System.Windows.Forms.Label();
            this.label_role = new System.Windows.Forms.Label();
            this.textBox_role = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_ip
            // 
            this.label_ip.AutoSize = true;
            this.label_ip.Location = new System.Drawing.Point(13, 13);
            this.label_ip.Name = "label_ip";
            this.label_ip.Size = new System.Drawing.Size(61, 13);
            this.label_ip.TabIndex = 0;
            this.label_ip.Text = "IP Address:";
            // 
            // textBox_ip
            // 
            this.textBox_ip.Location = new System.Drawing.Point(95, 6);
            this.textBox_ip.Name = "textBox_ip";
            this.textBox_ip.Size = new System.Drawing.Size(136, 20);
            this.textBox_ip.TabIndex = 1;
            // 
            // label_hostname
            // 
            this.label_hostname.AutoSize = true;
            this.label_hostname.Location = new System.Drawing.Point(13, 43);
            this.label_hostname.Name = "label_hostname";
            this.label_hostname.Size = new System.Drawing.Size(58, 13);
            this.label_hostname.TabIndex = 2;
            this.label_hostname.Text = "Hostname:";
            // 
            // textBox_hostname
            // 
            this.textBox_hostname.Location = new System.Drawing.Point(95, 36);
            this.textBox_hostname.Name = "textBox_hostname";
            this.textBox_hostname.Size = new System.Drawing.Size(188, 20);
            this.textBox_hostname.TabIndex = 3;
            // 
            // textBox_loginuser
            // 
            this.textBox_loginuser.Location = new System.Drawing.Point(95, 88);
            this.textBox_loginuser.Name = "textBox_loginuser";
            this.textBox_loginuser.Size = new System.Drawing.Size(188, 20);
            this.textBox_loginuser.TabIndex = 4;
            // 
            // label_loginuser
            // 
            this.label_loginuser.AutoSize = true;
            this.label_loginuser.Location = new System.Drawing.Point(12, 95);
            this.label_loginuser.Name = "label_loginuser";
            this.label_loginuser.Size = new System.Drawing.Size(65, 13);
            this.label_loginuser.TabIndex = 5;
            this.label_loginuser.Text = "Logon User:";
            // 
            // textBox_domainname
            // 
            this.textBox_domainname.Location = new System.Drawing.Point(95, 62);
            this.textBox_domainname.Name = "textBox_domainname";
            this.textBox_domainname.Size = new System.Drawing.Size(188, 20);
            this.textBox_domainname.TabIndex = 6;
            // 
            // label_domainname
            // 
            this.label_domainname.AutoSize = true;
            this.label_domainname.Location = new System.Drawing.Point(12, 69);
            this.label_domainname.Name = "label_domainname";
            this.label_domainname.Size = new System.Drawing.Size(77, 13);
            this.label_domainname.TabIndex = 7;
            this.label_domainname.Text = "Domain Name:";
            // 
            // label_role
            // 
            this.label_role.AutoSize = true;
            this.label_role.Location = new System.Drawing.Point(13, 121);
            this.label_role.Name = "label_role";
            this.label_role.Size = new System.Drawing.Size(32, 13);
            this.label_role.TabIndex = 8;
            this.label_role.Text = "Role:";
            this.label_role.Visible = false;
            // 
            // textBox_role
            // 
            this.textBox_role.Location = new System.Drawing.Point(95, 121);
            this.textBox_role.Name = "textBox_role";
            this.textBox_role.Size = new System.Drawing.Size(136, 20);
            this.textBox_role.TabIndex = 9;
            this.textBox_role.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 163);
            this.Controls.Add(this.textBox_role);
            this.Controls.Add(this.label_role);
            this.Controls.Add(this.label_domainname);
            this.Controls.Add(this.textBox_domainname);
            this.Controls.Add(this.label_loginuser);
            this.Controls.Add(this.textBox_loginuser);
            this.Controls.Add(this.textBox_hostname);
            this.Controls.Add(this.label_hostname);
            this.Controls.Add(this.textBox_ip);
            this.Controls.Add(this.label_ip);
            this.Name = "Form1";
            this.Text = "PCinfo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_ip;
        private System.Windows.Forms.TextBox textBox_ip;
        private System.Windows.Forms.Label label_hostname;
        private System.Windows.Forms.TextBox textBox_hostname;
        private System.Windows.Forms.TextBox textBox_loginuser;
        private System.Windows.Forms.Label label_loginuser;
        private System.Windows.Forms.TextBox textBox_domainname;
        private System.Windows.Forms.Label label_domainname;
        private System.Windows.Forms.Label label_role;
        private System.Windows.Forms.TextBox textBox_role;
    }
}

