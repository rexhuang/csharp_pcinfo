﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Security.Principal;
using System.Threading;

namespace PCinfo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String hostname = Dns.GetHostName();
            //String hostname = Environment.MachineName; 
            AppDomain domain = Thread.GetDomain();
            domain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            WindowsPrincipal principal = (WindowsPrincipal)Thread.CurrentPrincipal;
            textBox_hostname.Text = hostname;
            textBox_loginuser.Text = Environment.UserName;
            textBox_domainname.Text = Environment.UserDomainName;
            
            //IPAddress[] addrlist = Dns.GetHostAddresses(Dns.GetHostName());
            //IPAddress[] addrlist = Dns.GetHostByName(Dns.GetHostName()).AddressList; //過時
            IPHostEntry addrlist = Dns.GetHostEntry(hostname);

            foreach (IPAddress ipaddr in addrlist.AddressList)
            {
                //ipv4
                if (ipaddr.AddressFamily == AddressFamily.InterNetwork)
                {
                    textBox_ip.Text = ipaddr.ToString();
                }
            }

            //判斷出來結果怪怪的
            if (principal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                textBox_role.Text = "Administrator";
            } else if (principal.IsInRole(WindowsBuiltInRole.User)) {
                textBox_role.Text = "User";
            }
            else
            {
                textBox_role.Text = "Other";
            }
            
            
        }
    }
}
